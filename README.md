# 1. Update the apt package index:
```
$ sudo apt-get update

```
# 2. Install packages to allow apt to use a repository over HTTPS:
```
$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
```
# 3. Add Docker’s official GPG key:

```
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

# 4. Verify finger print
```
$ sudo apt-key fingerprint 0EBFCD88

```
# 5. Add docker ce repository:
```
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```

# 6. Install docker CE:
```
$ sudo apt-get install docker-ce docker-ce-cli containerd.io
```

# 7. Verify that Docker CE is installed correctly by running the hello-world image.
```
$ sudo docker run hello-world
```

