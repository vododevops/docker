# Install nginx with name 
```
[root@localhost ~]# docker run -d --name kiwi nginx:lastest

```

# Install mailer app:
```
[root@localhost ~]# docker run -d --name orange dockerinaction/ch2_mailer
```

# Check two container installed: 
```
[root@localhost ~]# docker ps
CONTAINER ID        IMAGE                       COMMAND                  CREATED             STATUS              PORTS               NAMES
d9bbe571ac33        dockerinaction/ch2_mailer   "/mailer/mailer.sh"      13 minutes ago      Up 13 minutes       33333/tcp           orange
3e3f1fad25ad        nginx:latest                "nginx -g 'daemon ..."   16 minutes ago      Up 16 minutes       80/tcp              kiwi
```

# Run a watcher container remember and keep interative with ctrl+P then Q
```
[root@localhost ~]# docker run -it --link kiwi:insideweb --link orange:insidemailer --name zawa dockerinaction/ch2_agent 
Unable to find image 'dockerinaction/ch2_agent:latest' locally
Trying to pull repository docker.io/dockerinaction/ch2_agent ... 
latest: Pulling from docker.io/dockerinaction/ch2_agent

[root@localhost ~]# docker exec zawa env
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
HOSTNAME=fda78abe2ddd
INSIDEWEB_PORT=tcp://172.17.0.3:80
INSIDEWEB_PORT_80_TCP=tcp://172.17.0.3:80
INSIDEWEB_PORT_80_TCP_ADDR=172.17.0.3
INSIDEWEB_PORT_80_TCP_PORT=80
INSIDEWEB_PORT_80_TCP_PROTO=tcp
INSIDEWEB_NAME=/zawa/insideweb
INSIDEWEB_ENV_NGINX_VERSION=1.17.1
INSIDEWEB_ENV_NJS_VERSION=0.3.3
INSIDEWEB_ENV_PKG_RELEASE=1~stretch
INSIDEMAILER_PORT=tcp://172.17.0.4:33333
INSIDEMAILER_PORT_33333_TCP=tcp://172.17.0.4:33333
INSIDEMAILER_PORT_33333_TCP_ADDR=172.17.0.4
INSIDEMAILER_PORT_33333_TCP_PORT=33333
INSIDEMAILER_PORT_33333_TCP_PROTO=tcp
INSIDEMAILER_NAME=/zawa/insidemailer
HOME=/home/example

[root@localhost ~]# docker exec zawa cat /watcher/watcher.sh
#!/bin/sh
while true
do
        if `printf "GET / HTTP/1.0\n\n" | nc -w 2 $INSIDEWEB_PORT_80_TCP_ADDR $INSIDEWEB_PORT_80_TCP_PORT | grep -q '200 OK'`
        then
		echo "System up."
        else 
        	printf "To: admin@work  Message: The service is down!" | nc $INSIDEMAILER_PORT_33333_TCP_ADDR $INSIDEMAILER_PORT_33333_TCP_PORT
                break
	fi

	sleep 1
done


```

# Test kiwi container down. Why zawa is missed?
```
[root@localhost ~]# docker stop kiwi
kiwi
[root@localhost ~]# docker ps
CONTAINER ID        IMAGE                       COMMAND               CREATED             STATUS              PORTS               NAMES
d9bbe571ac33        dockerinaction/ch2_mailer   "/mailer/mailer.sh"   40 minutes ago      Up 40 minutes       33333/tcp           orange
```

# Why docker isolated container with host:

```
[root@localhost ~]# docker exec orange ps
PID   USER     COMMAND
    1 example  {mailer.sh} /bin/sh /mailer/mailer.sh
   27 example  nc -l -p 33333
   28 example  ps

[root@localhost ~]# docker run --pid host busybox:latest ps | head -10
PID   USER     TIME  COMMAND
    1 root      0:08 /usr/lib/systemd/systemd --switched-root --system --deserialize 22
    2 root      0:00 [kthreadd]
    3 root      0:02 [ksoftirqd/0]
    5 root      0:00 [kworker/0:0H]
    7 root      0:00 [migration/0]
    8 root      0:00 [rcu_bh]
    9 root      0:08 [rcu_sched]
   10 root      0:00 [lru-add-drain]
   11 root      0:00 [watchdog/0]
```

# Check docker ps status all: 
```
[root@localhost ~]# docker ps
CONTAINER ID        IMAGE                       COMMAND               CREATED             STATUS              PORTS               NAMES
d9bbe571ac33        dockerinaction/ch2_mailer   "/mailer/mailer.sh"   59 minutes ago      Up 59 minutes       33333/tcp           orange


[root@localhost ~]# docker ps -a
CONTAINER ID        IMAGE                       COMMAND                  CREATED             STATUS                         PORTS               NAMES
a0af7b7b9e3e        busybox:latest              "ps"                     11 minutes ago      Exited (0) 11 minutes ago                          jolly_euclid
c6d34c9a788f        busybox:latest              "ps"                     12 minutes ago      Exited (0) 12 minutes ago                          determined_meninsky
26787206d85b        busybox:latest              "sh"                     12 minutes ago      Exited (0) 12 minutes ago                          silly_minsky
fd59d09b8049        dockerinaction/ch2_agent    "/watcher/watcher.sh"    22 minutes ago      Exited (0) 19 minutes ago                          zawa
d9bbe571ac33        dockerinaction/ch2_mailer   "/mailer/mailer.sh"      59 minutes ago      Up 59 minutes                  33333/tcp           orange
3e3f1fad25ad        nginx:latest                "nginx -g 'daemon ..."   About an hour ago   Exited (0) 19 minutes ago                          kiwi
42a27cc00de6        hello-world                 "/hello"                 About an hour ago   Exited (0) About an hour ago                       inspiring_spence
```